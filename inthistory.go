package inthistory

import (
	"bytes"
	"errors"
	"fmt"
	"time"
)

// Package errors
var (
	ErrInvalidMaxLen = errors.New("invalid max length")
	ErrHistoryEmpty  = errors.New("history empty")
)

// History holds a history of ints.
type History struct {
	Entries    Entries `db:"-"`
	Name       string  `db:"name"`
	MaxLen     int     `db:"maximum_length"`
	CurOrdinal int     `db:"current_ordinal"`
}

// Entries represents a slice of history entries
type Entries []Entry

type Stringer interface {
	String() string
}

func (entries Entries) String() string {
	if len(entries) == 0 {
		return "No entries"
	}

	buf := bytes.Buffer{}

	for _, entry := range entries {
		// buf.WriteString(fmt.Sprintf("(%d) %d ", entry.Ordinal, entry.Int))
		fmt.Fprintf(&buf, "(%d) %d ", entry.Ordinal, entry.Int)
	}

	return buf.String()
}

// Entry represents a history entry.
type Entry struct {
	Int     int       `db:"number" json:"int"`
	AddedAt time.Time `db:"created_at" json:"added_at"`
	Ordinal int       `db:"ordinal" json:"ordinal"`
}

// New creates and initializes a new history with maximum length maxLen.
func New(maxLen int) (*History, error) {
	if maxLen <= 0 {
		return nil, ErrInvalidMaxLen
	}

	return &History{
		Entries:    make([]Entry, 0),
		MaxLen:     maxLen,
		CurOrdinal: 0,
	}, nil
}

// Add adds a new entry to the history.
func (h *History) Add(i int) {
	h.Entries = append(h.Entries,
		Entry{
			Int:     i,
			AddedAt: time.Now(),
			Ordinal: h.CurOrdinal,
		})
	h.CurOrdinal++

	if h.Len() > h.MaxLen {
		h.Entries = h.Entries[1 : h.MaxLen+1]
	}
}

// Len returns the number of entries in the history.
func (h *History) Len() int {
	return len(h.Entries)
}

// DeleteOldest deletes the oldest entry from history and returns a copy of it.
func (h *History) DeleteOldest() (Entry, error) {
	if h.Len() == 0 {
		return Entry{}, ErrHistoryEmpty
	}

	delEntry := h.Entries[0]

	h.Entries = h.Entries[1:h.Len()]

	return delEntry, nil
}

// Clear clears the history.
func (h *History) Clear() error {
	if h.Len() == 0 {
		return ErrHistoryEmpty
	}

	h.Entries = h.Entries[:0]

	return nil
}
