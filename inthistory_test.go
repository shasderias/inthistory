package inthistory_test

import (
	"testing"

	history "gitlab.com/shasderias/inthistory"
)

func TestNew(t *testing.T) {
	if _, err := history.New(0); err != history.ErrInvalidMaxLen {
		t.Errorf("expected New(0) to return %v, instead got %v", history.ErrInvalidMaxLen, err)
	}

	if _, err := history.New(-1); err != history.ErrInvalidMaxLen {
		t.Errorf("expected New(-1) to return %v, instead got %v", history.ErrInvalidMaxLen, err)
	}

	hist, err := history.New(3)
	if err != nil {
		t.Errorf("expected New(3) to not return an error, instead got %v", err)
	}

	if len := hist.Len(); len != 0 {
		t.Errorf("expected New(3) to create a History with no entires, instead got %d entries", len)
	}

	if len := hist.MaxLen(); len != 3 {
		t.Errorf("expected New(3) to create a History with maximum length 3, instead got %d", len)
	}
}

type testEntry struct {
	Int     int
	Ordinal int
}

type testEntries []testEntry

func (te testEntries) Eq(he []history.Entry) bool {
	if len(te) != len(he) {
		return false
	}

	for i := 0; i < len(te); i++ {
		if (te[i].Int != he[i].Int) ||
			(te[i].Ordinal != he[i].Ordinal) {
			return false
		}
	}

	return true
}

func TestOperations(t *testing.T) {
	testCases := []struct {
		Init            func(h *history.History)
		ExpectedEntries testEntries
	}{
		{
			func(h *history.History) {
				h.Add(5)
			},
			testEntries{{5, 0}},
		},
		{
			func(h *history.History) {
				h.Add(5)
				h.Add(7)
				h.Add(10)
			},
			testEntries{{5, 0}, {7, 1}, {10, 2}},
		},
		{
			func(h *history.History) {
				h.Add(5)
				h.Add(7)
				h.Add(10)
				h.Add(24)
			},
			testEntries{{7, 1}, {10, 2}, {24, 3}},
		},
		{
			func(h *history.History) {
				h.Add(5)
				h.Add(7)
				h.Add(10)
				h.Add(24)
				h.DeleteOldest()
			},
			testEntries{{10, 2}, {24, 3}},
		},
		{
			func(h *history.History) {
				h.Add(5)
				h.Add(7)
				h.Add(10)
				h.Add(24)
				h.Clear()
			},
			testEntries{},
		},
	}

	for _, tc := range testCases {
		hist, err := history.New(3)
		if err != nil {
			t.Fatal("unexpected error", err)
		}

		tc.Init(hist)
		if !tc.ExpectedEntries.Eq(hist.Entries) {
			t.Errorf("expected %v, got %v", tc.ExpectedEntries, hist.Entries)
		}
	}
}

func TestStringer(t *testing.T) {
	testCases := []struct {
		Init        func(h *history.History)
		ExpectedStr string
	}{
		{
			func(h *history.History) {
			},
			"No entries",
		},
		{
			func(h *history.History) {
				h.Add(5)
				h.Add(7)
			},
			"(0) 5 (1) 7 ",
		},
	}

	for _, tc := range testCases {
		hist, err := history.New(3)
		if err != nil {
			t.Fatal("unexpected error", err)
		}

		tc.Init(hist)
		if tc.ExpectedStr != hist.Entries.String() {
			t.Errorf("expected %v, got %v", tc.ExpectedStr, hist.Entries.String())
		}
	}
}

func TestDeleteOldestErr(t *testing.T) {
	hist, err := history.New(3)
	if err != nil {
		t.Fatal("unexpected error", err)
	}

	if _, err = hist.DeleteOldest(); err != history.ErrHistoryEmpty {
		t.Fatalf("expected error %v, got %v", history.ErrHistoryEmpty, err)
	}
}
func TestClearErr(t *testing.T) {
	hist, err := history.New(3)
	if err != nil {
		t.Fatal("unexpected error", err)
	}

	if err = hist.Clear(); err != history.ErrHistoryEmpty {
		t.Fatalf("expected error %v, got %v", history.ErrHistoryEmpty, err)
	}
}
